# Rewriting Semantics and Type Checking for Session Type Calculus

This is the repository for the Maude implementation of rewriting semantics and algorithmic type checking for spi and HOp calculus. The repository is organized as follows:

```
├── spi
│   ├── chan.maude
│   ├── chanset.maude
│   ├── cinni.maude
│   ├── spi-syntax.maude
│   ├── spi-semantics.maude
│   ├── spi-types.maude
│   ├── spi-preds.maude
│   ├── spi-check.maude
│   ├── test1.maude
│   ├── test2.maude
├── hospi
│   ├── chan.maude
│   ├── chanset.maude
│   ├── cinni.maude
│   ├── hospi-syntax.maude
│   ├── hospi-semantics.maude
│   ├── hospi-types-syntax.maude
│   ├── hospi-types-func.maude
│   ├── hoppi-types.maude
│   ├── hospi-preds.maude
│   ├── hospi-check.maude
│   ├── test.maude
│   ├── test-check.maude

```

The spi folder contains the files associated to the implementation for spi calculus whereas hospi folder is composed of the files for hospi calculus. To perform spi examples run:

```sh
maude spi/test.maude
maude spi/test2.maude
```

To perform the hospi examples run:

```sh
maude hospi/test.maude
maude hospi/test-check.maude
```
